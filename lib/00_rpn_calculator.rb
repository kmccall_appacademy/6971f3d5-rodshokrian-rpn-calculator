class RPNCalculator

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def pop(num)
    @stack.pop
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def value
    @stack[-1]
  end

  def tokens(string)
    tokens = string.split(" ")
    tokens.map {|char| operation?(char) ? char.to_sym : char.to_i}
  end

  def evaluate(string)
    tokens = tokens(string)
    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end
    value
  end

  private def operation?(char)
    [:+, :-, :*, :/].include?(char.to_sym)
  end

  private def perform_operation(operator)
    raise "calculator is empty" if @stack.length < 2
    second_operand = @stack.pop
    first_operand = @stack.pop
    case operator
    when :+
      @stack << first_operand + second_operand
    when :-
      @stack << first_operand - second_operand
    when :*
      @stack << first_operand * second_operand
    when :/
      @stack << first_operand.fdiv(second_operand)
    else
      @stack << first_operand
      @stack << second_operand
      raise "Unrecognized operation: #{operator}"
    end

    "calculator is empty"
  end
end
